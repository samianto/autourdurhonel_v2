<?php
session_start();
$options = array(PDO::ATTR_AUTOCOMMIT=>FALSE);
$pdo = new PDO('mysql:host=localhost;dbname=adr', 'root', '');

$req = $pdo->prepare("SELECT * FROM membres WHERE id=:id AND archive=0");
$req->execute(["id"=>$_GET["id"]]);
$rep=$req->fetch();

if ($rep!=null){
$date = date_create($rep["since"]);
$date= date_format($date, 'j F Y');

?>

<p id=titre><?=$rep["firstName"]?> <?=$rep["name"]?></p>

<div id=include class="row">
    <div class="col-xs-12 col-md-8">
      <img style="width:100%" src="<?=$rep["img"]?>" alt="avatar de <?=$rep["firstName"]?> <?=$rep["name"]?>">
    </div>
    <div class="col-xs-12 col-md-4">
      <p><?=$rep["job"]?></p>
      <p>membre depuis le <?=$date?></p>
      <p><?=$rep["contact"]?></p>
      <p><?=$rep["description"]?></p>
    </div>
  </div>
<?php
}
