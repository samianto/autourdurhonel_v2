<?php

session_start();
$options = array(PDO::ATTR_AUTOCOMMIT=>FALSE);
$pdo = new PDO('mysql:host=localhost;dbname=adr', 'root', '');

$user=htmlentities($_POST['userName']);
$pass=htmlentities($_POST['pass']);

$req = $pdo->prepare("SELECT * FROM membres WHERE userName=:userName AND archive=0");
$req->execute(["userName"=>$user]);
$rep=$req->fetch();
if(!$rep){
  echo json_encode(["userName"=>"echec"]);
}
else{
  if ($rep && password_verify($pass, $rep['password'])) {
    $perm=explode(",",substr($rep['perm'],1,7));
    $_SESSION["token"]="open";
    $_SESSION["id"]=$rep["id"];
    $_SESSION["perm"]=$perm;
    echo json_encode(["validation"=>"succes"]);
  }
  else {
    echo json_encode(["validation"=>"echec"]);
  }
}
