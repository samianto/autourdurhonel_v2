<footer class="page-footer font-small blue pt-4 border-top">
  <div class="container-fluid text-center text-md-left">
    <div class="row">
      <div class="col-md-6 mt-md-0 mt-3">
        <h5 class="text-uppercase">Site info</h5>
        <p>Site crée et designé par Sylvain Amianto(Cql) pour l'association Autour du Rhonel.</p>
        <p>Réaliser à titre gratuit en échange de la diffusion des <a href="https://gitlab.com/samianto/autourdurhonel_v2">sources</a> sur <a href="https://gitlab.com/">gitLab</a></p>
      </div>
      <hr class="clearfix w-100 d-md-none pb-3">
      <div class="col-md-3 mb-md-0 mb-3">
        <h5 class="text-uppercase">Autour du rhonel</h5>
        <ul class="list-unstyled">
          <li>
            <a href="https://www.facebook.com/pages/category/Nonprofit-Organization/Autour-du-Rhonel-110476577111078/"><img style="width:50px" src="/img/site/fb.png" alt="logo facebook"> retrouver nous sur facebook</a>
          </li>
          <li>
            <a href="#">CGU</a>
          </li>
          <li>
            <a href="#">plan</a>
          </li>
        </ul>
      </div>
      <div class="col-md-3 mb-md-0 mb-3">
        <h5 class="text-uppercase">partenaire et autre</h5>
        <ul class="list-unstyled">
          <li>
            <a href="https://icons8.com/icon/116717/photo-gallery">Photo Gallery icon by Icons8</a>
          </li>
          <li>
            <a href="https://getbootstrap.com/"><img style="width:50px" src="/img/site/bootstrap.png" alt="logo bootstrap"> bootStrap</a>
          </li>
          <li>
            <a href="https://www.smarty.net/"><img style="width:50px" src="/img/site/smarty.webp" alt="logo smarty"> Smarty</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <div class="footer-copyright text-center py-3">
    © 2020 Copyright: Toute reproduction, en tout ou en partie, sous quelque forme que ce soit, est interdite sans l'autorisation préalable de l'auteur.
    <?php
    if (!isset($_SESSION["token"])) {
      echo "<img class='float-right' width=50 src='/img/icone/setting.png' alt='open setting' onclick='$(`#modalConnex`).modal(`toggle`)'>";
    }else{
      echo "<img class='float-right' width=50 src='/img/icone/close1.png' alt='close setting' onclick='disconnect()'>";
    }
    ?>
  </div>
  <!-- Modal connection-->
  <div class="modal fade" id="modalConnex" tabindex="-1" role="dialog" aria-labelledby="modalConnexLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modalConnexLabel">Connexion</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id=formConnex>
            <div class="form-group">
              <label for="userName">Nom d'utilisateur</label>
              <input type="text" class="form-control" id="userName" name="userName" aria-describedby="userHelp" placeholder="Nom d'utilisateur">
              <small id="userHelp" class="form-text text-muted">Veuillez saisir votre nom d'utilisateur.</small>
            </div>
            <div class="form-group">
              <label for="pass">mot de passe</label>
              <input type="password" class="form-control" id="pass" name="pass" placeholder="mot de passe">
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <div id='formConnectAlertDanger' class="alert alert-danger d-none" role="alert"></div>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" onclick="prepareForm()">Connexion</button>
        </div>
      </div>
    </div>
  </div>
</footer>
