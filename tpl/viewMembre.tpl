<div class="container-fluid mt-3 px-3">
  <section>
    <div class="row">
      <div style="background-color:#ece6dc; font-family: 'Niconne', cursive;" class="offset-1 col-10 px-3 py-3 mb-3 rounded-pill">
        <!-- bouton ajout uniquement visible pour admin membre -->
        {if isset($perm) && $perm eq 3}
        <button style="font-Size: 1.5rem" type="button" class="btn btn-secondary float-right rounded-pill"> + Ajouter</button>
        {/if}
        <h1>Nos membres</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-md-10 col-xs-12 offset-md-1 offset-xs-0">
        <div class="row">
          <!-- //boucle affichage carte membre -->
          {if $membres eq null}
          <div class="col-12">
            <p>Aucun membre à afficher</p>
          </div>
          {else}
          {foreach from=$membres item=membre}
          <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-3">
            <div class="card">
              <img src="{$membre->getImg()}" class="card-img-top" alt="avatar de {$membre->getFirstName()}">
              <div class="card-body">
                <h5 class="card-title">{$membre->getFirstName()} {$membre->getName()}</h5>
                <h5>{$membre->getJob()}</h5>
                <p class="card-text">Membre depuis le {$membre->getSince()|date_format:" %e %B, %Y"}</p>
              </div>
              <div class="card-footer d-flex flex-inline justify-content-between">
                {if (isset($perm) && $perm eq 3) || (isset($monId) && $monId eq $membre->getId())}
                <button type="button" class="btn btn-secondary rounded" onclick="genModal({$membre->getId()})"> Editer</button>
                {else}
                <button type="button" class="btn btn-secondary rounded" onclick="ajaxDom('/ajax/viewMembre.php?id='+{$membre->getId()}, showModal);"> Afficher</button>
                {/if}
                {if isset($perm) && $perm eq 3}
                <button type="button" class="btn btn-secondary rounded"> archiver</button>
                {/if}
              </div>
            </div>
          </div>
          {/foreach}
          {/if}
          <!-- fin de la boucle -->
        </div>
      </div>
    </div>
  </section>
  {if isset($perm) && $perm eq 3}
  <section class="mt-3">
    <div class="row">
      <div style="background-color:#ece6dc; font-family: 'Niconne', cursive;" class="offset-1 col-10 px-3 py-3 mb-3 rounded-pill">
        <button style="font-Size: 1.5rem" type="button" class="btn btn-secondary float-right rounded-pill">Tout supprimer</button>
        <h1>Archives</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-md-10 col-xs-12 offset-md-1 offset-xs-0">
        <div class="row">
          <!-- boucle affichage des membres archivé -->
          {if !isset($ArchivedMembres) || $ArchivedMembres eq null}
          <div class="col-12">
            <p>Aucun membre archivé à afficher</p>
          </div>
          {else}
          {foreach from=$ArchivedMembres item=archivedMembre}
          <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-3">
            <div class="card">
              <img src="{$archivedMembre->getImg()}" class="card-img-top" alt="avatar de {$archivedMembre->getFirstName()}">
              <div class="card-body">
                <h5 class="card-title">{$archivedMembre->getFirstName()} {$archivedMembre->getName()}</h5>
                <h5>{$archivedMembre->getJob()}</h5>
                <p class="card-text">Membre depuis le {$archivedMembre->getSince()|date_format:" %e %B, %Y"}</p>
              </div>
              <div class="card-footer d-flex justify-content-between">
                <button type="button" class="btn btn-secondary rounded"> Restaurer</button>
                <button type="button" class="btn btn-secondary rounded"> Supprimer</button>
              </div>
            </div>
          </div>
          {/foreach}
          {/if}
          <!-- fin de la boucle -->
        </div>
à    </div>
    </div>
  </section>
  {/if}
</div>
<!-- fenetre Modal -->
<div class="modal fade" id="modalMembre" tabindex="-1" role="dialog" aria-labelledby="titreModal" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <!-- modifier le titre de la modal -->
        <h5 class="modal-title" id="titreModal">EDIT_ME</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div id="bodyModal" class="modal-body">

        <!-- //inserer le resultat de al requete ajax ici -->



      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
      {if isset($perm) && $perm eq 3}
      <button type="button" class="btn btn-primary">Supprimer</button>
      {/if}
    </div>
  </div>
</div>
