<?php
class Controller {

	public $input;
	public $files;
	var $vars = array();
	var $erreur = array();
	var $succes = array();
	var $log = array();

	public function __construct() {
		if (isset($_POST)) {
			$this->input = array_map("htmlentities", $_POST);
		}
		if (isset($_FILES)) {
			$this->files = $_FILES;
		}
	}

	function loadDao($name) {
		require_once('dao/'.$name.'.dao.php');
		$daoClass = 'Dao'.$name;
		$this->$daoClass = new $daoClass();
	}
}
