<?php
require(ROOT."model/Membres.class.php");

class DaoMembres{
    function getMembres(){
      $req=DB::request("SELECT * FROM membres WHERE archive=0 AND job!='webmaster' ORDER BY perm DESC");
      if($req)return self::createObject($req);
      else return null;
    }

    function read($id){
      $req=DB::request("SELECT * FROM membres WHERE archive=0 AND id=:id", ["id"=>$id]);
      if($req)return self::createObject($req);
      else return null;
    }
    function readArchive($id){
      $req=DB::request("SELECT * FROM membres WHERE archive=1 AND id=:id", ["id"=>$id]);
      if($req)return self::createObject($req);
      else return null;
    }


    function readAll(){
      $req=DB::request("SELECT * FROM membres WHERE archive=0 AND job!='webmaster'");
      if($req)return self::createObject($req);
      else return null;
    }
    function readAllArchive(){
      $req=DB::request("SELECT * FROM membres WHERE archive=1 AND job!='webmaster'");
      if($req)return self::createObject($req);
      else return null;
    }





    function add($newMembre){
      $req=DB::request("INSERT INTO membres (userName,password,name,
        firstName,img,description,job,contact,perm) VALUES(:UserName,:Password,:Name,
        :FirstName,:img,:Description,:Job,:Contact,:Perm)",[
        "UserName"=>$newMembre->getUserName(),"Password"=>$newMembre->getPassword(),
        "Name"=>$newMembre->getName(),"FirstName"=>$newMembre->getFirstName(),
        "img"=>$newMembre->getImg(),"Description"=>$newMembre->getDescription(),
        "Job"=>$newMembre->getJob(),"Contact"=>$newMembre->getContact(),
        "Perm"=>$newMembre->getPerm()]);
        return DB::lastId();

    }

    function update($membre){
    $req=DB::request("UPDATE membres set userName=:userName,password=:password,
        name=:name,firstName=:firstName,img=:img,description=:description,
        since=:since,job=:job,contact=:contact,perm=:perm,archive=:archive
        WHERE id=:id",[
          "id"=>$membre->getId(),
          "userName"=>$membre->getUserName(),
          "password"=>$membre->getPassword(),
          "name"=>$membre->getName(),
          "firstName"=>$membre->getFirstName(),
          "img"=>$membre->geTimg(),
          "description"=>$membre->getDescription(),
          "since"=>$membre->getSince(),
          "job"=>$membre->getJob(),
          "contact"=>$membre->getContact(),
          "perm"=>$membre->getPerm(),
          "archive"=>$membre->getArchive(),
        ]);
        return true;
    }






    function checkUserName($newUserName,$userName){
      $req=DB::request("SELECT id FROM membres WHERE userName=:newUserName AND userName!=:userName",["newUserName"=>$newUserName,"userName"=>$userName]);
        if($req)return true;
        else return false;
    }

    function verifPass($id,$pass){
      $req=DB::request('SELECT password FROM membres WHERE id=:id',["id"=>$id]);
      if (isset($req[0]) && password_verify($pass, $req[0]['password'])) {
        return true;
      }
      else{
        return false;
      }
    }


//fonction interne
function createObject($req){
  foreach ($req as $i => $data) {
    $rep[$i]=new Membre(
    $data['id'],
    $data['userName'],
    $data['password'],
    $data['name'],
    $data['firstName'],
    $data['img'],
    $data['description'],
    $data['since'],
    $data['job'],
    $data['contact'],
    $data['perm'],
    $data['archive'],
    );
  }
  return $rep;
}


}
