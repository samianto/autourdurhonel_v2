<?php
require(ROOT.'model/Press.class.php');
class DaoPress{

  function getRand($q){
    $req=DB::request("SELECT article.id,firstName,name,title,content,article.img,style,author,dateAdd,article.archive
      FROM article
      INNER JOIN membres ON article.author=membres.id
      WHERE article.archive=0 ORDER BY RAND() LIMIT :nbr", ["nbr"=>$q]);
    if($req) return self::createObjet($req);
    else return null;
  }

  function readAll(){
    $req=DB::request("SELECT article.id,firstName,name,title,content,article.img,style,author,dateAdd,article.archive
      FROM article
      INNER JOIN membres ON article.author=membres.id
      WHERE article.archive=0 ORDER BY article.dateAdd");
      if($req) return self::createObjet($req);
    else return null;
  }
  function readAllArchive(){
    $req=DB::request("SELECT article.id,firstName,name,title,content,article.img,style,author,dateAdd,article.archive
      FROM article
      INNER JOIN membres ON article.author=membres.id
      WHERE article.archive=1 ORDER BY article.dateAdd");
      if($req) return self::createObjet($req);
    else return null;
  }





  function read($id){
    $req=DB::request("SELECT article.id,firstName,name,title,content,article.img,style,author,dateAdd,article.archive
      FROM article
      INNER JOIN membres ON article.author=membres.id
      WHERE article.archive=0 AND article.id=:id",["id"=>$id]);
      if($req) return self::createObjet($req);
    else return null;
  }
  //fonction interne
  function createObjet($req){
    foreach ($req as $i => $data) {
      $rep[$i]=new Article(
        $data['id'],
        $data['firstName']." ".$data['name'],
        $data['title'],
        $data['content'],
        $data['img'],
        $data['style'],
        $data['author'],
        $data['dateAdd'],
        $data['archive']
      );
    }
    return $rep;
  }
}
