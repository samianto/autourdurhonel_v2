<?php
require(ROOT."model/Activite.class.php");
class DaoActivite{
  public function getRand($q){
    $req=DB::request("SELECT * FROM activity WHERE archive=0 AND end>NOW() ORDER BY RAND() LIMIT :nb",["nb"=>$q]);
    if($req) return self::createObject($req);
    else return null;
  }
  public function readAll(){
    $req=DB::request("SELECT * FROM activity WHERE archive=0 AND end>NOW() ORDER BY dateAdd");
    if($req) return self::createObject($req);
    else return null;
  }
  public function read($id){
    $req=DB::request("SELECT * FROM activity WHERE archive=0 AND id=:id ", ["id"=>$id]);
    if($req) return self::createObject($req);
    else return null;
  }



  //fonction interne
  function createObject($req){
    foreach ($req as $i => $data) {
      $rep[$i]=new Activite($data['id'],$data['title'],$data['comment'],
      $data['img'],$data['addBy'],$data['dateAdd'],$data['end'],$data['archive']
      );
    }
    return $rep;
  }
}
