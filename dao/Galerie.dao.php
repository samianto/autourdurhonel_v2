<?php
require(ROOT."model/Galerie.class.php");
class DaoGalerie{
  function getSlider(){
    $req=db::request("SELECT * FROM galerie WHERE onSlider=1 AND archive=0");

    if($req) return self::createObject($req);
    else return null;
  }


  function getRand($q){
    $req=DB::request("SELECT * FROM galerie WHERE type=1 AND archive=0 AND onGalerie=1 ORDER BY RAND() LIMIT :nbr", array("nbr"=>$q));
    if($req) return self::createObject($req);
    else return null;
  }



  function getGalerie(){
    $req=db::request("SELECT * FROM galerie WHERE onGalerie=1 AND archive=0");
    if($req) return self::createObject($req);
    else return null;
  }

  function readAll(){
    $req=db::request("SELECT * FROM galerie WHERE archive=0");
    if ($req) return self::createObject($req);
    else return null;
  }
  function readAllArchive(){
    $req=db::request("SELECT * FROM galerie WHERE archive=1");
    if ($req) return self::createObject($req);
    else return null;
  }



  function read($id){
    $req=db::request("SELECT galerie.id,title,path,comment,addBy,dateAdd,type,preview,onSlider,onGalerie,galerie.archive,name,firstName
      FROM galerie
      INNER JOIN membres ON galerie.addBy=membres.id
      WHERE galerie.archive=0 AND galerie.id=:id",["id"=>$id]);
      if($req) {
        $data=self::createObject($req);
        $data[0]->setProprietaire($req[0]["firstName"]." ".$req[0]["name"]);
        return $data;
      }
      else return null;
    }

    function readArchive($id){
      $req=db::request("SELECT * FROM galerie WHERE archive=1 AND id=:id",["id"=>$id]);
        if($req) return self::createObject($req);
        else return null;
      }

    function add($objet){
      $req=db::request("INSERT INTO galerie (title,path,comment,addBy,type,preview,onSlider,onGalerie)
      VALUES(:title,:path,:comment,:addBy,:type,:preview,:onSlider,:onGalerie) ",
      [
        "title"=>$objet->getTitle(),
        "path"=>$objet->getPath(),
        "comment"=>$objet->getComment(),
        "addBy"=>$objet->getAddBy(),
        "type"=>$objet->getType(),
        "preview"=>$objet->getPreview(),
        "onSlider"=>$objet->getOnSlider(),
        "onGalerie"=>$objet->getOnGalerie()
      ]
    );
    if($req) return true;
  }

  function update($object){
    $req=db::request("UPDATE galerie SET title=:title,comment=:comment,
      preview=:preview,onSlider=:onSlider,onGalerie=:onGalerie,archive=:archive
      WHERE id=:id",[
        "title"=>$object->getTitle(),
        "comment"=>$object->getComment(),
        "preview"=>$object->getPreview(),
        "onSlider"=>$object->getOnSlider(),
        "onGalerie"=>$object->getOnGalerie(),
        "archive"=>$object->getArchive(),
        "id"=>$object->getId()
      ]);
      return true;
  }




  //fonction interne
  function createObject($req){
    foreach ($req as $key => $data)
    {
      $rep[$key]=new galerie(
        $data['id'],$data['title'],$data['path'],$data['comment'],
        $data['addBy'],$data['dateAdd'],$data['type'],$data['preview'],
        $data['onSlider'],$data['onGalerie'],$data['archive']
      );
    }
    return $rep;
  }




}
