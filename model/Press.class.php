<?php
class Article{

  private $id;
  private $authorName;
  private $title;
  private $content;
  private $img;
  private $style;
  private $author;
  private $dateAdd;
  private $archive;

  public function __construct($id,$authorName,$title,$content,$img,$style,$author,$dateAdd,$archive){
    $this->id=$id;
    $this->authorName=$authorName;
    $this->title=$title;
    $this->content=$content;
    $this->img=$img;
    $this->style=$style;
    $this->author=$author;
    $this->dateAdd=$dateAdd;
    $this->archive=$archive;
  }

  public function getId(){return $this->id;}
  public function getAuthorName(){return $this->authorName;}
  public function getTitle(){return $this->title;}
  public function getContent(){return $this->content;}
  public function getImg(){return $this->img;}
  public function getStyle(){return $this->style;}
  public function getAuthor(){return $this->author;}
  public function getDateAdd(){return $this->dateAdd;}
  public function getArchive(){return $this->archive;}

  public function SetId($id){$this->id=$id;}
  public function SetAuthorName($authorName){$this->authorName=$authorName;}
  public function SetTitle($title){$this->title=$title;}
  public function SetContent($content){$this->content=$content;}
  public function SetImg($img){$this->img=$img;}
  public function SetStyle($style){$this->style=$style;}
  public function SetAuthor($author){$this->author=$author;}
  public function SetDateAdd($dateAdd){$this->dateAdd=$dateAdd;}
  public function SetArchive($archive){$this->archive=$archive;}
}
