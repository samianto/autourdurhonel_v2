<?php
class Galerie{
  private $id;
  private $title;
  private $path;
  private $comment;
  private $addBy;
  private $dateAdd;
  private $type;
  private $preview;
  private $onSlider;
  private $onGalerie;
  private $archive;
  private $proprietaire=null;

  public function __construct($id,$title,$path,$comment,$addBy,$dateAdd,$type,$preview,$onSlider,$onGalerie,$archive){
    $this->id=$id;
    $this->title=$title;
    $this->path=$path;
    $this->comment=$comment;
    $this->addBy=$addBy;
    $this->dateAdd=$dateAdd;
    $this->type=$type;
    $this->preview=$preview;
    $this->onSlider=$onSlider;
    $this->onGalerie=$onGalerie;
    $this->archive=$archive;
  }

  public function getId(){return $this->id;}
  public function getTitle(){return $this->title;}
  public function getPath(){return $this->path;}
  public function getComment(){return $this->comment;}
  public function getAddBy(){return $this->addBy;}
  public function getDateAdd(){return $this->dateAdd;}
  public function getType(){return $this->type;}
  public function getPreview(){return $this->preview;}
  public function getOnSlider(){return $this->onSlider;}
  public function getOnGalerie(){return $this->onGalerie;}
  public function getArchive(){return $this->archive;}
  public function getProprietaire(){return $this->proprietaire;}

  public function setId($id){$this->id=$id;}
  public function setTitle($title){$this->title=$title;}
  public function setPath($path){$this->path=$path;}
  public function setComment($comment){$this->comment=$comment;}
  public function setAddBy($addBy){$this->addBy=$addBy;}
  public function setDateAdd($dateAdd){$this->dateAdd=$dateAdd;}
  public function setType($type){$this->type=$type;}
  public function setPreview($preview){$this->preview=$preview;}
  public function setOnSlider($onSlider){$this->onSlider=$onSlider;}
  public function setOnGalerie($onGalerie){$this->onGalerie=$onGalerie;}
  public function setArchive($archive){$this->archive=$archive;}
  public function setProprietaire($proprietaire){$this->proprietaire=$proprietaire;}
}
