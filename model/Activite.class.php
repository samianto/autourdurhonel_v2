<?php
class Activite{

  private $id;
  private $title;
  private $comment;
  private $img;
  private $addBy;
  private $dateAdd;
  private $end;
  private $archive;

  public function __construct($id,$title,$comment,$img,$addBy,$dateAdd,$end,$archive){
    $this->id=$id;
    $this->title=$title;
    $this->comment=$comment;
    $this->img=$img;
    $this->addBy=$addBy;
    $this->dateAdd=$dateAdd;
    $this->end=$end;
    $this->archive=$archive;
  }

  public function getId(){return $this->id;}
  public function getTitle(){return $this->title;}
  public function getComment(){return $this->comment;}
  public function getImg(){return $this->img;}
  public function getAddBy(){return $this->addBy;}
  public function getDateAdd(){return $this->dateAdd;}
  public function getEnd(){return $this->end;}
  public function getArchive(){return $this->archive;}

  public function SetId($id){$this->id=$id;}
  public function SetTitle($title){$this->title=$title;}
  public function SetComment($comment){$this->comment=$comment;}
  public function SetImg($img){$this->img=$img;}
  public function SetAddBy($addBy){$this->addBy=$addBy;}
  public function SetDateAdd($dateAdd){$this->dateAdd=$dateAdd;}
  public function SetEnd($end){$this->end=$end;}
  public function SetArchive($archive){$this->archive=$archive;}

}
