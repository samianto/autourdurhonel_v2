<?php
class Membre{

private $id;
private $userName;
private $password;
private $name;
private $firstName;
private $img;
private $description;
private $since;
private $job;
private $contact;
private $perm;
private $archive;

public function __construct($id,$userName,$password,$name,$firstName,$img,$description,$since,$job,$contact,$perm,$archive){
  $this->id=$id;
  $this->name=$name;
  $this->userName=$userName;
  $this->password=$password;
  $this->firstName=$firstName;
  $this->img=$img;
  $this->description=$description;
  $this->since=$since;
  $this->job=$job;
  $this->contact=$contact;
  $this->perm=$perm;
  $this->archive=$archive;
}

public function getId(){return $this->id;}
public function getUserName(){return $this->userName;}
public function getPassword(){return $this->password;}
public function getName(){return $this->name;}
public function getFirstName(){return $this->firstName;}
public function getImg(){return $this->img;}
public function getDescription(){return $this->description;}
public function getSince(){return $this->since;}
public function getJob(){return $this->job;}
public function getContact(){return $this->contact;}
public function getPerm(){return $this->perm;}
public function getArchive(){return $this->archive;}

public function setId($id){$this->id=$id;}
public function setUserName($userName){$this->userName=$userName;}
public function setPassword($password){$this->password=$password;}
public function setName($name){$this->name=$name;}
public function setFirstName($firstName){$this->firstName=$firstName;}
public function setImg($img){$this->img=$img;}
public function setDescription($description){$this->description=$description;}
public function setSince($since){$this->since=$since;}
public function setJob($job){$this->job=$job;}
public function setContact($contact){$this->contact=$contact;}
public function setPerm($perm){$this->perm=$perm;}
public function setArchive($archive){$this->archive=$archive;}

}
