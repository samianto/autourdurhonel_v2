<?php
session_start();
setlocale (LC_TIME, 'fr_FR','fra');
//definition des constentes
define('WEBROOT',str_replace('index.php', '', $_SERVER['SCRIPT_NAME']));
define('ROOT',str_replace('index.php', '', $_SERVER['SCRIPT_FILENAME']));
// ajout des dependences
require_once('dependencies/smarty-3.1.34/libs/smarty.class.php');
require_once('core/bdd.php');
require_once('core/controller.php');
require_once('dependencies/FileUpload.class.php');
// dependense de debug a supprimer en prod
require(ROOT."dependencies/dumpData.php");
//tableau des controlleurs autoriser
$tabControlleur = array("Accueil", "Membre", "Galerie", "Reportages", "Activite", "Contact");
//definition de la page par default
if(!isset($_GET['p'])||empty($_GET['p']))
$_GET['p'] = "Accueil/index";
//recuperation du controlleur et de la methode associée
$param = explode("/",$_GET['p']);
if (in_array($param[0], $tabControlleur)){
  $controllerName = $param[0];
  if (!isset($param[1])||empty($param[1])) $action = 'index';
  else $action = $param[1];
  // chargement du fichier du controlleur et instence du controlleur
  require_once('controller/'.$controllerName.'.ctrl.php');
  $controller = 'Ctrl'.$controllerName;
  $controller = new $controller();
  if (method_exists($controller,$action)){
    ?>
    <!doctype html>
    <html lang="fr">
    <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <!-- script pour recaptcha -->
      <script src="https://www.google.com/recaptcha/api.js" async defer></script>
      <!-- Bootstrap CSS -->
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
      <!-- autre css -->
      <link rel="stylesheet" href="<?=WEBROOT?>css/nav.css">
      <link rel="stylesheet" href="<?=WEBROOT?>css/footer.css">
      <!-- css charger pour la page en cour -->
      <?php
      echo "<link rel='stylesheet' href='".WEBROOT."css/".$controllerName."/".$action.".css'>";
      ?>
      <!-- polices (googleFont) -->
      <link href="https://fonts.googleapis.com/css2?family=Niconne&display=swap" rel="stylesheet">
      <!-- titre de l'onglet -->
      <?php
      if($action!='index')
      echo "<title>$controllerName - $action</title>";
      else
      echo "<title>$controllerName</title>";
      ?>
      <!-- script pour inclure le captcha -->
      <script type="text/javascript">
      var onloadCallback = function() {
        grecaptcha.render(document.getElementById('divCapcha'), {
          'sitekey' : '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI'
        });
      };
      </script>
    </head>

    <?php
    // body
    if (!isset($_SESSION["token"]) || $_SESSION["token"]!="open") echo "<body>";
    else echo "<body style='border: dashed 5px #fb7684'>";
    // nav
    require("includes/nav.html");
    ?>
    <main>
      <?php
      unset($param[0]);
      unset($param[1]);
      call_user_func_array(array($controller,$action), $param);
      ?>
    </main>
    <?php
    // footer
    require("includes/footer.php");
    ?>

    <!-- Optional JavaScript -->
    <script src="<?=WEBROOT?>js/main/navBar.js"></script>
    <script src="<?=WEBROOT?>js/main/methodAjax.js"></script>
    <script src="<?=WEBROOT?>js/main/connection.js"></script>
    <script src="<?=WEBROOT?>js/<?=$controllerName?>/<?=$action?>.js"></script>

    </script>
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
  </body>
  </html>
  <?php
}
else header("location:".WEBROOT."errM");
}
else header("location:".WEBROOT."errC");
?>
