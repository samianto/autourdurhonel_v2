<?php
class FileUpload
{
	//attribut
	private $mimeList=[
		"document"=>["application/pdf","application/zip","application/vnd.oasis.opendocument.text","application/vnd.oasis.opendocument.spreadsheet","application/vnd.oasis.opendocument.presentation","application/vnd.oasis.opendocument.graphics","application/vnd.ms-excel","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application/vnd.ms-powerpoint","application/vnd.openxmlformats-officedocument.presentationml.presentation","application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document","text/plain"],
		"audio"=>["audio/mpeg","audio/mp3","audio/x-ms-wma","audio/x-wav","audio/wav"],
		"image"=>["image/gif","image/jpeg","image/png","image/vnd.microsoft.icon","image/svg+xml"],
		"video"=>["video/mpeg","video/mp4","video/x-ms-wmv","video/x-msvideo","video/x-flv","video/web"]
	];
	private $extentionList=[
		"document"=>["pdf","zip","odt","odt","odp","odg","xls","xlsx","ppt","pptx","doc","docx","txt"],
		"audio"=>["mpeg","mp3","wma","wav"],
		"image"=>["gif","jpeg","jpg","png","ico","svg"],
		"video"=>["mpeg","mp4","wmv","avi","flv","mkv"]
	];
	private $maxSize;
	private $path;
	private $mimeValid=[];
	private $extensionValid=[];
	private $succes=0;

	private $nbrFiles;
	private $name=[];
	private $type;
	private $tmp_name=[];
	private $error=[];
	private $size=[];

	private $extention=[];
	//private $resize;
	private $log=[];
	private $transfert=[];
	//constructeur

	public function __construct($data, $path, $type=null, $maxSize=null)
	{
		//verif du 1er paramètre
		if(!isset($data)||$data==NULL || !is_array($data))
		{
			$this->log["FATAL_ERROR"][]="le 1er paramètre n'est pas valide";
			// return $this->log;
			return false;
		}
		else
		{
			$this->log["constructeur"][]="la liste de fichier a bien été reçu";
		}
		//verif du second parametre
		if($path{strlen($path)-1}!="/")
		{
			$path=$path."/";
		}
		if(!file_exists($path))
		{
			$this->log["FATAL_ERROR"][]="Le dossier \"$path\" n'existe pas";
			// return $this->log;
			return false;
		}
		else
		{
			$this->log["constructeur"][]="Le dossier \"$path\" a bien été trouvé";
		}
		if(!is_writable($path))
		{
			$this->log["FATAL_ERROR"][]="vous n'avez pas les droits pour écrire dans le dossier \"$path\"";
			// return $this->log;
			return false;
		}
		else
		{
			$this->log["constructeur"][]="vous avez bien les droits pour écrire dans ce dossier";
		}
		//comptage des fichiers

		if (is_array($data['name'])) //envoie par formulaire multiple
		{
			$this->nbrFiles=count($data['name']);
			$this->log["constructeur"][]="le formulaire est de type envoie multiple et contiens ".$this->nbrFiles." fchier(s)";
			foreach ($data['tmp_name'] as $value)
			{
				$this->transfert[$value]=true;
			}
		}
		else //envoie par formulaire simple
		{
			$this->log["constructeur"][]="le formulaire ne permet d'envoyer qu'un seul fichier";
			$data=["name"=>[$data['name']],"type"=>[$data['type']],"tmp_name"=>[$data['tmp_name']],"error"=>[$data['error']],"size"=>[$data['size']]];
			$this->nbrFiles=count($data['name']);
			foreach ($data["tmp_name"] as $value)
			{
				$this->transfert[$value]=true;
			}
		}


		//verification du parametre type
		if(isset($type)&&!is_array($type)) $type=explode(",", $type);
		elseif(isset($type)&&is_array($type)) $type=$type;
		else $type=["document","audio","image","video"];

		foreach ($type as $key=>$value)
		{
			if(!in_array($value,["document","image","audio","video"]))
			{
				$this->log["constructeur"][]="[echec] le 3eme paramètre contiens une valeur invalide : \".$type[$key].\"";
				unset($type[$key]);
			}
		}


		//declaration des conditions de l'upload
		if (isset($maxSize))
		{
			if (!is_int($maxSize))
			{
				$this->maxSize=0;
				$this->log["constructeur"][]="[ATTENTION] parametre de la taille des fichiers incorrecte. la taille n'est pas limitée";
			}
			else
			{
			$this->maxSize=(($maxSize*1024)*1024);
			$this->log["constructeur"][]="la taille des fichiers est limitée à ".$maxSize."Mo (".($maxSize*1024*1024)." octets)";
			}
		}
		else
		{
			$this->maxSize=0;
			$this->log["constructeur"][]="la taille des fichiers n'est pas limitée";
		}
		$this->path=$path;
		foreach ($type as  $value) $this->mimeValid = array_merge($this->mimeValid,$this->mimeList[$value]);
		foreach ($type as  $value) $this->extensionValid = array_merge($this->extensionValid,$this->extentionList[$value]);
		$this->log["constructeur"][]="les extension autorisé sont ".implode(', ', $this->extensionValid);

		// declaration des information du/des fichier(s) envoyé(s)
		$this->hydrate($data);

		//check des erreurs
		$this->errorFile($this->tmp_name,$this->error);
		$this->checkSize($this->tmp_name,$this->maxSize,$this->size);
		$this->checkMime($this->tmp_name,$this->type,$this->mimeValid);
		$this->checkExten($this->tmp_name,$this->extension,$this->extensionValid);
	}

	//method
	public function upload()
	{
		if(isset($this->log['FATAL_ERROR']))
		{
			$this->log['upload'][]="[ECHEC] le transfére de fichier est impossible due a une erreur falale : ".implode("", $this->log['FATAL_ERROR']);
			return false;
		}
		else
		{
			foreach ($this->tmp_name as $key => $value)
			{
				if($this->transfert[$value]==true)
				{
					$this->log["upload"][]="[SUCCES] le fichier ".$this->name[$key]." va etre transféré dans le dossier ".$this->path;
					$this->name[$key]=md5(uniqid(rand(), true)).'.'.$this->extension[$key];
					$this->log["upload"][]="[SUCCES] ............le fichier a été renommer en ".$this->name[$key];
				if($transfert = move_uploaded_file($value,$this->path.$this->name[$key]))//upload de l'image
				{
					$this->log["upload"][]="[SUCCES] ............le fichier à bien été transféré";
					$this->succes++;
				}
				else
				{
					$this->log["upload"][]="[ECHEC] ............une erreur c'est produite au transfert du fichier";
					return false;
				}
			}
			else
			{
				$this->log["upload"][]="[ECHEC] ............le fichier ".$this->name[$key]." ne va pas etre transféré";
				unset($this->name[$key]);
				return false;
			}
		}
		$this->log["upload"][]="[SUCCES] transfert terminé. ".$this->succes."/".$this->nbrFiles." fichier(s) transféré(s)";
			//verification du contenu du fichier
		foreach ($this->name as $key=>$value)
		{
			if($this->verifScript($this->path.$value, $key))return true;
		}
	}
}

private function hydrate($data)
{
	$this->setName($data['name']);
	$this->setType($data['type']);
	$this->setTmp_name($data['tmp_name']);
	$this->setError($data['error']);
	$this->setSize($data['size']);
	foreach ($data['name'] as $value)
	{
		$this->setExtension(pathinfo($value, PATHINFO_EXTENSION));
	}
}


// verif le code erreur de la variable FILES
private function errorFile($file, $error)
{
	foreach ($error as $key=>$value)
	{
		$i=$file[$key];
		switch ($value)
		{
			case 1:
			$this->log["errorFile"][]="[ECHEC] La taille du fichier téléchargé (".$this->name[$key].") excède la valeur maximum configurée dans le serveur.";
			$this->transfert[$i]=false;
			break;
			case 2:
			$this->log["errorFile"][]="[ECHEC] La taille du fichier téléchargé (".$this->name[$key].") excède la valeur spécifiée par le formulaire";
			$this->transfert[$i]=false;
			break;
			case 3:
			$this->log["errorFile"][]="[ECHEC] ".$this->name[$key]." n'a été que partiellement téléchargé";
			$this->transfert[$i]=false;
			break;
			case 4:
			$this->log["errorFile"][]="[ECHEC] ".$this->name[$key]." n'a pas été téléchargé.";
			$this->transfert[$i]=false;
			break;
			case 6:
			$this->log["errorFile"][]="[ECHEC] Un dossier temporaire est manquant";
			$this->transfert[$i]=false;
			break;
			case 7:
			$this->log["errorFile"][]="[ECHEC] Échec de l'écriture du fichier ".$this->name[$key]." sur le disque";
			$this->transfert[$i]=false;
			break;
			case 8:
			$this->log["errorFile"][]="[ECHEC] Une erreur c'est produite pendent le téléchargement du fichier".$this->name[$key];
			$this->transfert[$i]=false;
			break;
			case 0:
			$this->log["errorFile"][]="[SUCCES] Le fichier ".$this->name[$key]." a bien été téléchargé dans le dossier temporaire";
			break;
		}
	}
}

// verif le poids
private function checkSize($file,$sizeMax,$size)
{
	foreach ($size as $key=>$value)
	{
		$i=$file[$key];
		if ($sizeMax==0)
		{
			$this->log["checkSize"][]="aucune taille maximum de fichier n'a été specifié";
		}
		else
		{
			if($value>$sizeMax)
			{
				$this->log["checkSize"][]="[ECHEC] le fichier transféré (".$this->name[$key].") est trop gros.";
				$this->transfert[$i]=false;
			}
			else
			{
				$this->log["checkSize"][]="[SUCCES] le fichier (".$this->name[$key].") correspond aux critères de taille.";
			}
		}
	}
}
// verif mime
private function checkMime($file,$type, $mimeValid)
{
	foreach ($type as $key=>$value)
	{
		$i=$file[$key];
		if (!in_array($value,$mimeValid))
		{
			$this->log["mimeCheck"][]="[ECHEC] le type MIME du fichier (".$this->type[$key].") n'est pas autorisé";
			$this->transfert[$i]=false;
		}
		else
		{
			$this->log["mimeCheck"][]="[SUCCES] le type MIME du fichier (".$this->type[$key].") est autorisé";
		}
	}
}
// verif extention
private function checkExten($file,$extension, $extensionValid)
{
	foreach ($extension as $key=>$value)
	{
		$i=$file[$key];
		$value=strtolower($value);
		if (!in_array($value,$extensionValid))
		{
			$this->log["checkExten"][]="[ECHEC] l'extention' du fichier (".$this->extension[$key].") n'est pas autorisée";
			$this->transfert[$i]=false;
		}
		else
		{
			$this->log["checkExten"][]="[SUCCES]l'extention' du fichier (".$this->type[$key].") est autorisée";
		}
	}
}
	// verif anti script
private function verifScript($fichier, $key)
{
		$file = fopen($fichier, 'r+');//on ouvre le fichier
		while($ligne=fgets($file))
		{

			// if(stristr($ligne, 'script') == TRUE) //on verifi si chaque ligne contiens le mot cle "script"
			if(preg_match('/<\s*s\s*c\s*r\s*i\s*p\s*t\s*/',$ligne)==true)
			{
				unlink($fichier);
				$this->log["verifScript"][]="[DANGER]le fichier contient du code malveillant et a été supprimé";
				unset($this->name[$key]);
				return false;
			}
			else return true;
		}
	}
	//getteur
	public function getname(){return $this->name;}
	public function getMimeValid(){return $this->mimeValid;}
	public function getMime(){return $this->type;}
	public function getextensionValid(){return$this->extensionValid;}
	public function getExtension(){return $this->extension;}
	public function getType(){return $this->type;}
	public function getSize(){return$this->size;}
	public function getLog(){return$this->log;}
	public function getPath(){return $this->path;}
	//public function getTransfert(){return$this->transfert;}
	//setteur

	private function setMimeValid($mime){$this->mimeVailde=$mime;}
	private function setextensionValid($extention){$this->extensionsValides=$extention;}
	private function setName($name){$this->name=$name;}
	private function setType($type){$this->type=$type;}
	private function setTmp_name($tmp_name){$this->tmp_name=$tmp_name;}
	private function setError($error){$this->error=$error;}
	private function setSize($size){$this->size=$size;}
	private function setExtension($ext){$this->extension[]=$ext;}
	public function setPath($path){$this->path=$path;}

}
