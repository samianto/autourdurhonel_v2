<?php
function dump($var)
{
	ob_start();
	echo "<pre>";
	print_r($var);
	echo "</pre>";
	echo ob_get_clean();
}
