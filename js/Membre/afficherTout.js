function showModal(data){
  // methode apeler en ajax depuis la template sur onclick
  var titreModal=document.getElementById("titreModal");
  var bodyModal=document.getElementById("bodyModal");
  titreModal.innerHTML=data.getElementById("titre").innerHTML;

  if(bodyModal.childNodes){
    var elms=bodyModal.childNodes;
    while(elms.length>0){
      bodyModal.removeChild(bodyModal.lastChild);
    }
  }

  bodyModal.appendChild(data.getElementById("include"));
  $('#modalMembre').modal('toggle')
}
