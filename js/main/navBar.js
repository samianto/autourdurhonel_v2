window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  var subTitle=document.getElementById('subTitle');
  var listImg=document.querySelectorAll("nav img");


  if (document.body.scrollTop > 120 || document.documentElement.scrollTop > 120) {
    listImg[0].style.width = "30px";
    listImg[0].style.height = "30px";
    listImg[0].classList.replace("align-middle","align-top");
    subTitle.classList.replace("d-inline","d-none")


    for (var i = 1; i < listImg.length; i++) {
      listImg[i].classList.replace("d-inline-block","d-none");
    }



  } else {
    listImg[0].style.width = "150px";
    listImg[0].style.height = "150px";
    listImg[0].classList.replace("align-top","align-middle");
    subTitle.classList.replace("d-none","d-inline")
    for (var i = 1; i < listImg.length; i++) {
      listImg[i].classList.replace("d-none","d-inline-block");
    }
  }
}
