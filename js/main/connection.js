function prepareForm(){
  var formConnex = document.getElementById('formConnex');
  var inputs=formConnex.querySelectorAll('input');
  if(inputs[0].value=="" || inputs[1].value==""){
    console.log("champ vide");
    notif=document.getElementById("formConnectAlertDanger");
    notif.innerHTML="Veuillez remplir les champs"
    notif.classList.replace("d-none","d-Block")
    return;
  }
  formData = new FormData();
  formData.append("userName",inputs[0].value)
  formData.append("pass",inputs[1].value)
   ajaxSubmit("/ajax/connection.php",connectState, formData);
}


function disconnect(){
  ajax("/ajax/deco.php",connectState)
}

function connectState(data){
  notif=document.getElementById("formConnectAlertDanger");
  if(data.userName){
    if (data.userName=="echec") {
      notif.innerHTML="Nom d'utilisateur incorrect !"
      notif.classList.replace("d-none","d-Block")
      return;
    }
  }
  else if(data.validation){
    if (data.validation=="succes") {
      document.location.reload(true);
    }else {
      notif.innerHTML="Mot de passe incorrect !"
      notif.classList.replace("d-none","d-Block")
      return;
    }
  }
  else if(data.deconnexion){
    document.location.reload(true);
  }
}
