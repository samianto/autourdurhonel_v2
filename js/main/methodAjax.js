// pour submit un formulaire en async via javaScript le retour se fait en JSON
function ajaxSubmit(url,callback, data){
  var req = new XMLHttpRequest();
  req.responseType = 'json';
  req.open("POST", url);
  req.addEventListener("load", function ()
  {
    if (req.status >= 200 && req.status < 400){
      callback(req.response);
    }else{
      console.error(req.status + " " + req.statusText + " " + url);
      return false;
    }
  });
  req.addEventListener("error", function (){
    console.error("Erreur réseau avec l'URL " + url);
  });
  if (data) req.send(data);
  else req.send(null);
  return true;
}

// pour executer un fichier en async via javaScript le retour se fait en JSON
function ajax(url, callback){
  var req = new XMLHttpRequest();
  req.responseType = 'json';
  req.open("get", url);
  req.addEventListener("load", function (){
    if (req.status >= 200 && req.status < 400){
      callback(req.response);
    }
    else{
      console.error(req.status + " " + req.statusText + " " + url);
      return false;
    }
  });
  req.addEventListener("error", function (){
    console.error("Erreur réseau avec l'URL " + url);
  });
  req.send(null);
  return true;
}

// pour executer un fichier en async via javaScript le retour se fait en XML(DOM)
function ajaxDom(url, callback){
  var req = new XMLHttpRequest();
  req.responseType = 'document';
  req.open("get", url);
  req.addEventListener("load", function (){
    if (req.status >= 200 && req.status < 400){
      callback(req.response);
    }
    else{
      console.error(req.status + " " + req.statusText + " " + url);
      return false;
    }
  });
  req.addEventListener("error", function (){
    console.error("Erreur réseau avec l'URL " + url);
  });
  req.send(null);
  return true;
}
