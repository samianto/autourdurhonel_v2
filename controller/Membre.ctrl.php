<?php
class CtrlMembre extends controller{
  public function index(){
    header("location:/Membre/afficherTout");
    exit("ERREUR REDIRECTION");

  }

  public function AfficherTout(){
    $smarty=new smarty();
    $this->loadDao("Membres");
    $membres=$this->DaoMembres->readAll();
    $smarty->assign("membres",$membres);
    $membresArchive=$this->DaoMembres->readAllArchive();
    $smarty->assign("ArchivedMembres",$membresArchive);
    if(isset($_SESSION['token']) && $_SESSION["perm"][0]==3)
      $smarty->assign("perm",3);
    if(isset($_SESSION['token']))
      $smarty->assign("monId",$_SESSION['id']);
    $smarty->display("tpl/viewMembre.tpl");
  }





}
